function doGet(e){  
 var template = HtmlService.createTemplateFromFile('index');
 var decoded = Utilities.base64Decode(e.parameter.url);
 template.docUrl = Utilities.newBlob(decoded).getDataAsString();
 var page = template.evaluate();  
 return HtmlService.createHtmlOutput(page).setTitle('GDoc Trello Integration');
}
  
function res(param)
 {
   var body = DocumentApp.openByUrl(param.url).getBody();
   var searchType = DocumentApp.ElementType.PARAGRAPH;
   var searchHeading = DocumentApp.ParagraphHeading.HEADING1;
   var searchResult = null;
   var searchStr = DocumentApp.ParagraphHeading.NORMAL;
    
   var results = [];
   
   while (searchResult = body.findElement(searchType, searchResult)) {
    var item = {};
    var par = searchResult.getElement().asParagraph();
    if (par.getHeading() == searchHeading&&par.getText()!="") {
      
      item.title =par.getText();
    }
    if (par.getHeading() == searchStr&&par.getText()!="")
    {
        item = results.pop()
        if(item) {
          item.desc = par.getText();
        }
    }
    results.push(item);
   }

     var response =  JSON.stringify(results);
   
    return results; 
 }
  
